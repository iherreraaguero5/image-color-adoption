import axios from "@/api/config";
export const fetchRandomImage = (size = 700, params = {}) => {
  return new Promise((resolve, reject) => {
    axios
      .request({
        url: `${size}.jpg`,
        method: "get",
        params: params,
        responseType: "arrayBuffer",
        headers: { "Content-Type": "image/jpeg" },
      })
      .then((response) => {
        if (response.status !== 200) reject(response);
        else {
          resolve(response.request.responseURL);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};
